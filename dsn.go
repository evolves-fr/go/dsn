package dsn

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

var (
	ErrNotValidDSN        = errors.New("not valid DSN")
	ErrNotValidDSNPort    = errors.New("not valid DSN port")
	ErrNotValidDSNQueries = errors.New("not valid DSN queries")
)

func ParseDSN(raw string) (*DSN, error) {
	var dsn DSN

	if err := dsn.Unmarshal(raw); err != nil {
		return nil, err
	}

	return &dsn, nil
}

type DSN struct {
	Scheme   string
	Username string
	Password string
	Protocol string
	Endpoint string
	Port     int
	Path     string
	Queries  Queries
}

func (dsn *DSN) Unmarshal(raw string) error {
	// Prepare regex
	regex := regexp.MustCompile(
		`^(?:(?P<scheme>\w+)://)?` + // [scheme://]
			`(?:(?P<username>[^:@]*)(?:\:(?P<password>[^@]*))?@)?` + // [username[:password]@]
			`(?:(?P<protocol>tcp6?|udp6?)\()?(?P<endpoint>[^:\)/]*)(?:\:(?P<port>[^\)/?]+))?\)?` + // [protocol(endpoint[:port])]
			`(?:/(?P<path>[^\?]*))?` + // [/path]
			`(?:\?(?P<queries>.*))?$`) // [?param1=value1&paramN=valueN]

	// Check if valid DSN
	if !regex.MatchString(raw) {
		return ErrNotValidDSN
	}

	// Match regex with raw value
	match := regex.FindStringSubmatch(raw)

	// Set values
	dsn.Scheme = match[regex.SubexpIndex("scheme")]
	dsn.Username = match[regex.SubexpIndex("username")]
	dsn.Password = match[regex.SubexpIndex("password")]
	dsn.Protocol = match[regex.SubexpIndex("protocol")]
	dsn.Endpoint = match[regex.SubexpIndex("endpoint")]
	dsn.Path = match[regex.SubexpIndex("path")]

	var err error

	if value := match[regex.SubexpIndex("port")]; value != "" {
		dsn.Port, err = strconv.Atoi(value)
		if err != nil {
			return ErrNotValidDSNPort
		}
	}

	if value := match[regex.SubexpIndex("queries")]; value != "" {
		dsn.Queries, err = ParseQueries(value)
		if err != nil {
			return ErrNotValidDSNQueries
		}
	}

	return nil
}

func (dsn *DSN) UnmarshalJSON(raw []byte) error {
	return dsn.Unmarshal(strings.Trim(string(raw), "\""))
}

func (dsn *DSN) UnmarshalYAML(fn func(interface{}) error) error {
	var raw string

	_ = fn(&raw)

	return dsn.Unmarshal(raw)
}

func (dsn *DSN) UnmarshalTOML(raw interface{}) error {
	return dsn.Unmarshal(fmt.Sprintf("%v", raw))
}
