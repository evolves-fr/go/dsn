package dsn

import (
	"errors"
	"net/url"
)

var ErrNotValidQueries = errors.New("not valid queries")

func ParseQueries(raw string) (Queries, error) {
	queries, err := url.ParseQuery(raw)
	if err != nil {
		return nil, ErrNotValidQueries
	}

	return Queries(queries), nil
}

type Queries map[string][]string

func (q Queries) Has(key string) bool {
	_, exist := q[key]

	return exist
}

func (q Queries) Get(key string) string {
	if q == nil {
		return ""
	}

	value := q[key]

	if len(value) == 0 {
		return ""
	}

	return value[0]
}
