# Go DSN

Parse DSN (Data Source Name) URL (Uniform Resource Identifier)

## Usage

```go
example, err := dsn.ParseDSN("s3://clientID:secretID@s3.example.tld/my-bucket?tls=true")
if err != nil {
	panic(err)
}
fmt.Printf("%#v", example)
```
