package dsn_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/evolves-fr/go/dsn"
)

func TestParseQueries(t *testing.T) {
	for raw, res := range map[string]dsn.Queries{
		"param1=value1": {
			"param1": {"value1"},
		},
		"param1=value1&param1=value2": {
			"param1": {"value1", "value2"},
		},
		"param1=value1&param1=value2&param2=value3": {
			"param1": {"value1", "value2"},
			"param2": {"value3"},
		},
	} {
		t.Run(raw, func(t *testing.T) {
			ret, err := dsn.ParseQueries(raw)
			assert.NoError(t, err)
			assert.Equal(t, res, ret)
		})
	}

	t.Run("invalid", func(t *testing.T) {
		ret, err := dsn.ParseQueries("param1=value1;param2=value2")
		assert.Error(t, err)
		assert.Nil(t, ret)
	})
}

func TestQueries_Has(t *testing.T) {
	t.Run("exist", func(t *testing.T) {
		var test = dsn.Queries{"param1": {"value1"}}
		assert.Equal(t, true, test.Has("param1"))
	})

	t.Run("not-exist", func(t *testing.T) {
		var test = dsn.Queries{"param2": {"value2"}}
		assert.Equal(t, false, test.Has("param1"))
	})

	t.Run("not-exist", func(t *testing.T) {
		var test dsn.Queries
		assert.Equal(t, false, test.Has("param1"))
	})
}

func TestQueries_Get(t *testing.T) {
	t.Run("exist", func(t *testing.T) {
		var test = dsn.Queries{"param1": {"value1"}}
		assert.Equal(t, "value1", test.Get("param1"))
	})

	t.Run("not-exist", func(t *testing.T) {
		var test = dsn.Queries{"param2": {"value2"}}
		assert.Equal(t, "", test.Get("param1"))
	})

	t.Run("nil", func(t *testing.T) {
		var test dsn.Queries
		assert.Equal(t, "", test.Get("param1"))
	})
}
