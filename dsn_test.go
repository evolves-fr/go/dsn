package dsn_test

import (
	"encoding/json"
	"testing"

	"gopkg.in/yaml.v3"

	"github.com/BurntSushi/toml"
	"github.com/stretchr/testify/assert"

	"gitlab.com/evolves-fr/go/dsn"
)

func TestParseDSN(t *testing.T) {
	for raw, res := range map[string]dsn.DSN{
		"endpoint": {
			Scheme:   "",
			Username: "",
			Password: "",
			Protocol: "",
			Endpoint: "endpoint",
			Port:     0,
			Path:     "",
			Queries:  nil,
		},

		"endpoint:1234": {
			Scheme:   "",
			Username: "",
			Password: "",
			Protocol: "",
			Endpoint: "endpoint",
			Port:     1234,
			Path:     "",
			Queries:  nil,
		},

		"udp6(endpoint:1235)": {
			Scheme:   "",
			Username: "",
			Password: "",
			Protocol: "udp6",
			Endpoint: "endpoint",
			Port:     1235,
			Path:     "",
			Queries:  nil,
		},

		"endpoint:1236/path": {
			Scheme:   "",
			Username: "",
			Password: "",
			Protocol: "",
			Endpoint: "endpoint",
			Port:     1236,
			Path:     "path",
			Queries:  nil,
		},

		"endpoint:1237/path?param1=value1": {
			Scheme:   "",
			Username: "",
			Password: "",
			Protocol: "",
			Endpoint: "endpoint",
			Port:     1237,
			Path:     "path",
			Queries: dsn.Queries{
				"param1": {"value1"},
			},
		},

		"endpoint:1238/path?param1=value1&param2=value2": {
			Scheme:   "",
			Username: "",
			Password: "",
			Protocol: "",
			Endpoint: "endpoint",
			Port:     1238,
			Path:     "path",
			Queries: dsn.Queries{
				"param1": {"value1"},
				"param2": {"value2"},
			},
		},

		"endpoint:1239/path?param1=multi1&param1=multi2&param2=multi3": {
			Scheme:   "",
			Username: "",
			Password: "",
			Protocol: "",
			Endpoint: "endpoint",
			Port:     1239,
			Path:     "path",
			Queries: dsn.Queries{
				"param1": {"multi1", "multi2"},
				"param2": {"multi3"},
			},
		},

		"endpoint:1240?param1=value1": {
			Scheme:   "",
			Username: "",
			Password: "",
			Protocol: "",
			Endpoint: "endpoint",
			Port:     1240,
			Path:     "",
			Queries: dsn.Queries{
				"param1": {"value1"},
			},
		},

		"endpoint:1241?param1=value1&param2=value2": {
			Scheme:   "",
			Username: "",
			Password: "",
			Protocol: "",
			Endpoint: "endpoint",
			Port:     1241,
			Path:     "",
			Queries: dsn.Queries{
				"param1": {"value1"},
				"param2": {"value2"},
			},
		},

		"endpoint:1242?param1=multi1&param1=multi2&param2=multi3": {
			Scheme:   "",
			Username: "",
			Password: "",
			Protocol: "",
			Endpoint: "endpoint",
			Port:     1242,
			Path:     "",
			Queries: dsn.Queries{
				"param1": {"multi1", "multi2"},
				"param2": {"multi3"},
			},
		},

		"mysql://endpoint": {
			Scheme:   "mysql",
			Username: "",
			Password: "",
			Protocol: "",
			Endpoint: "endpoint",
			Port:     0,
			Path:     "",
			Queries:  nil,
		},

		"mysql://user@endpoint": {
			Scheme:   "mysql",
			Username: "user",
			Password: "",
			Protocol: "",
			Endpoint: "endpoint",
			Port:     0,
			Path:     "",
			Queries:  nil,
		},

		"mysql://user:pass@endpoint": {
			Scheme:   "mysql",
			Username: "user",
			Password: "pass",
			Protocol: "",
			Endpoint: "endpoint",
			Port:     0,
			Path:     "",
			Queries:  nil,
		},

		"mysql://user:pass@tcp6(endpoint)": {
			Scheme:   "mysql",
			Username: "user",
			Password: "pass",
			Protocol: "tcp6",
			Endpoint: "endpoint",
			Port:     0,
			Path:     "",
			Queries:  nil,
		},

		"mysql://user:pass@tcp6(endpoint:9875)": {
			Scheme:   "mysql",
			Username: "user",
			Password: "pass",
			Protocol: "tcp6",
			Endpoint: "endpoint",
			Port:     9875,
			Path:     "",
			Queries:  nil,
		},

		"mysql://user:pass@tcp6(endpoint:9876)/my-path?param1=multi1&param1=multi2&param2=multi3": {
			Scheme:   "mysql",
			Username: "user",
			Password: "pass",
			Protocol: "tcp6",
			Endpoint: "endpoint",
			Port:     9876,
			Path:     "my-path",
			Queries: dsn.Queries{
				"param1": {"multi1", "multi2"},
				"param2": {"multi3"},
			},
		},

		"mysql://user:pass@tcp(192.168.1.1:9877)/my-path?param1=multi1&param1=multi2&param2=multi3": {
			Scheme:   "mysql",
			Username: "user",
			Password: "pass",
			Protocol: "tcp",
			Endpoint: "192.168.1.1",
			Port:     9877,
			Path:     "my-path",
			Queries: dsn.Queries{
				"param1": {"multi1", "multi2"},
				"param2": {"multi3"},
			},
		},
	} {
		t.Run(raw, func(t *testing.T) {
			ret, err := dsn.ParseDSN(raw)
			assert.NoError(t, err)
			assert.Equal(t, res, *ret)
		})
	}

	t.Run("invalid-dsn", func(t *testing.T) {
		ret, err := dsn.ParseDSN("i-nvalid://test.tld:1234")
		assert.Equal(t, dsn.ErrNotValidDSN, err)
		assert.Nil(t, ret)
	})

	t.Run("invalid-port", func(t *testing.T) {
		ret, err := dsn.ParseDSN("invalid://test.tld:fake")
		assert.Equal(t, dsn.ErrNotValidDSNPort, err)
		assert.Nil(t, ret)
	})

	t.Run("invalid-queries", func(t *testing.T) {
		ret, err := dsn.ParseDSN("invalid://test.tld/?param1=value1;param2=value2)")
		assert.Equal(t, dsn.ErrNotValidDSNQueries, err)
		assert.Nil(t, ret)
	})
}

func TestUnmarshalJSON(t *testing.T) {
	const req = `{"dsn": "scheme://username:password@udp(127.0.0.1:1234)/path?param1=value1&param1=value2&param2=value3"}`

	type config struct {
		DSN dsn.DSN `json:"dsn"`
	}

	var cfg config

	err := json.Unmarshal([]byte(req), &cfg)

	assert.NoError(t, err)
	assert.Equal(t, dsn.DSN{
		Scheme:   "scheme",
		Username: "username",
		Password: "password",
		Protocol: "udp",
		Endpoint: "127.0.0.1",
		Port:     1234,
		Path:     "path",
		Queries: dsn.Queries{
			"param1": {"value1", "value2"},
			"param2": {"value3"},
		},
	}, cfg.DSN)
}

func TestUnmarshalYAML(t *testing.T) {
	const req = `dsn: scheme://username:password@udp(127.0.0.1:1234)/path?param1=value1&param1=value2&param2=value3`

	type config struct {
		DSN dsn.DSN `yaml:"dsn"`
	}

	var cfg config

	err := yaml.Unmarshal([]byte(req), &cfg)

	assert.NoError(t, err)
	assert.Equal(t, dsn.DSN{
		Scheme:   "scheme",
		Username: "username",
		Password: "password",
		Protocol: "udp",
		Endpoint: "127.0.0.1",
		Port:     1234,
		Path:     "path",
		Queries: dsn.Queries{
			"param1": {"value1", "value2"},
			"param2": {"value3"},
		},
	}, cfg.DSN)
}

func TestUnmarshalTOML(t *testing.T) {
	const req = `dsn = "scheme://username:password@udp(127.0.0.1:1234)/path?param1=value1&param1=value2&param2=value3"`

	type config struct {
		DSN dsn.DSN `toml:"dsn"`
	}

	var cfg config

	_, err := toml.Decode(req, &cfg)

	assert.NoError(t, err)
	assert.Equal(t, dsn.DSN{
		Scheme:   "scheme",
		Username: "username",
		Password: "password",
		Protocol: "udp",
		Endpoint: "127.0.0.1",
		Port:     1234,
		Path:     "path",
		Queries: dsn.Queries{
			"param1": {"value1", "value2"},
			"param2": {"value3"},
		},
	}, cfg.DSN)
}
